﻿using UnityEngine;
using System.Collections;

//Controll firld marker position.
public class FieldSelectorController : MonoBehaviour
{
	public GameObject marker;

	void Update ()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit = new RaycastHit();

		if(Physics.Raycast(ray, out hit, Camera.main.farClipPlane, 1 << 19) && GameInputController.Instance.fieldSkillActive) //Raycast only ground (19th layer).
		{
			transform.position = hit.point;
			marker.SetActive(true);
		}
		else
		{
			marker.SetActive(false);
		}
	}
}