﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//SwitchCameraMode UI controller.
public class UISwitchCameraMode : MonoBehaviour
{
	//Button text.
	public Text buttonTex;

	//Initialization.
	public void Init()
	{
		buttonTex.text = "CamMode " + GameCameraController.Instance.currentMode.ToString ();
	}

	//Switch camera mode.
	public void SwitchCameraMode()
	{
		GameCameraController.Instance.currentMode++;
		
		if ((int)GameCameraController.Instance.currentMode > 1)
			GameCameraController.Instance.currentMode = 0;

		buttonTex.text = "CamMode " + GameCameraController.Instance.currentMode.ToString ();
	}

	public void Start()
	{
		Init ();
	}
}