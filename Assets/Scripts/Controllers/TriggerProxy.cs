﻿using UnityEngine;
using System;
using System.Collections;

//This class proxy trigger info to receiver.
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class TriggerProxy : MonoBehaviour
{
	//Receiver
	public MonoBehaviour receiver;

	//Reciever handler interface.
	private ITriggerHandler handler;

	//Rigidbody of the object.
	private Rigidbody rBody;
	//Sphere collider of the object.
	private SphereCollider sCollider;

	//Initialization;
	public void Init()
	{
		try
		{
			handler = receiver.GetComponent<ITriggerHandler>();
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: cant get acces to ITriggerHandler!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);

			enabled = false;
			return;
		}

		try
		{
			rBody = GetComponent<Rigidbody>();
			rBody.isKinematic = true;
			rBody.useGravity = false;
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			
			Debug.LogWarningFormat ("{0} - {1}: the object has not have a rigidbody!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
			enabled = false;
			return;
		}
		
		try
		{
			sCollider = GetComponent<SphereCollider>();
			sCollider.isTrigger = true;
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			
			Debug.LogWarningFormat ("{0} - {1}: the object has not have a sphere collider!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);

			enabled = false;
			return;
		}
	}

	//Handle collider call-back.
	public void OnTriggerStay(Collider other)
	{
		try
		{
			handler.OnCustomTriggerStay (other);
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: handler has not defined!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);
			
			enabled = false;
			return;
		}
	}

	void Start()
	{
		Init ();
	}
}