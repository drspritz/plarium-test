﻿using UnityEngine;
using System.Collections;

//Controll marker.
public class TargetMarkerController : MonoBehaviour
{
	//Singleton implementation.
	public static TargetMarkerController Instance { get; private set; }
	
	//Current light.
	public GameObject imageContainer;
	
	//Animator.
	public Animator anim;
	
	//Current selected object.
	private WorldObject selectedObject;
	
	//Get selected objec.
	public WorldObject getSelectedObject
	{
		get
		{
			return selectedObject;
		}
	}

	//Instance initialization.
	public void Awake()
	{
		if (Instance == null)
			Instance = this;
	}
	
	public void OnSelect(WorldObject inputObject)
	{
		selectedObject = inputObject;
		anim.Play ("targetMargker_fadeOut", 0, 0f);
	}
	
	public void OnDeselect()
	{
		selectedObject = null;
	}

	void Update ()
	{
		if(selectedObject != null)
		{
			Unit selected = null;

			try
			{
				selected = selectedObject as Unit;

				if(selected == null)
					return;
			}
			catch (System.Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: selected object not a Unit!",
				                        System.DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name);
				
				return;
			}

			WorldObject target = selected.getCurrentTarget;

			if(selected.group == PlayerManager.Instance.playerGroup && target != null)
			{
				transform.position = target.transform.position;
				imageContainer.SetActive(true);
			}
			else
			{

				imageContainer.SetActive(false);
			}
		}
		else
		{
			imageContainer.SetActive(false);
		}
	}
}