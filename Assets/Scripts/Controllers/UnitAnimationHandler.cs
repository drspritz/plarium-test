﻿using UnityEngine;
using System.Collections;

//Handle unit data and control animation.
public class UnitAnimationHandler : MonoBehaviour
{
	//Animator.
	public Animator anim;

	//Owner.
	public Unit owner;

	void Update ()
	{
		anim.SetFloat ("speed", Mathf.Abs(owner.navMeshVelocity.magnitude / owner.speed));
		anim.speed = Random.Range(0.1f, 0.3f) + Mathf.Abs(owner.navMeshVelocity.magnitude) * 0.4f;
	}
}