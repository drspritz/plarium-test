﻿using UnityEngine;
using System.Collections;

//Controll selectspot.
public class SpotSelectController : MonoBehaviour
{
	//Singleton implementation.
	public static SpotSelectController Instance { get; private set; }

	//Current light.
	public Light _light;

	//Animator.
	public Animator anim;

	//Current selected object.
	private WorldObject selectedObject;

	//Get selected objec.
	public WorldObject getSelectedObject
	{
		get
		{
			return selectedObject;
		}
	}

	//Instance initialization.
	public void Awake()
	{
		if (Instance == null)
			Instance = this;
	}
	
	public void OnSelect(WorldObject inputObject)
	{
		selectedObject = inputObject;
		anim.Play ("select_point_fadeOut", 0, 0f);
		_light.intensity = 8f;
	}

	public void OnDeselect()
	{
		selectedObject = null;
		_light.intensity = 0f;
	}

	public void Update()
	{
		if(selectedObject != null)
		{
			if(selectedObject.gameObject.activeInHierarchy && selectedObject.group == PlayerManager.Instance.playerGroup)
			{
				transform.position = selectedObject.transform.position;
				transform.rotation = selectedObject.transform.rotation;
			}
			else
			{
				GameInputController.Instance.OnDeselect();
			}
		}
		else
		{
			GameInputController.Instance.OnDeselect();
		}
	}
}