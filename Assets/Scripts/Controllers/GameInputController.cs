﻿using UnityEngine;
using System;
using System.Collections;

//Handle input mouse data and controll units.
public class GameInputController : InputHandler
{
	//Singleton implementation.
	public static GameInputController Instance { get; private set; }
	
	//Instance initialization.
	public void Awake()
	{
		if (Instance == null)
			Instance = this;
	}

	//Select some object (click has been on world object 20th layaer).
	protected override void OnSelect (RaycastHit hit)
	{
		base.OnSelect (hit);

		WorldObject selectedObject = null;

		try
		{
			selectedObject = hit.transform.GetComponent<WorldObject> ();

			if(selectedObject.group == PlayerManager.Instance.playerGroup)
			{
				PlayerManager.Instance.controllableObject = selectedObject as IControllable;
				UIManager.Instance.OnObjectSelected(selectedObject);
				SpotSelectController.Instance.OnSelect(selectedObject);
				TargetMarkerController.Instance.OnSelect(selectedObject);

				Debug.LogFormat ("{0} - {1}: selected new object - {2}!",
				                        DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name,
				                        selectedObject.name);
			}
			else
			{
				Debug.LogWarningFormat ("{0} - {1}: you cant select this object - {2}!",
				                        DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name,
				                        selectedObject.name);

			}
		}
		catch (Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: selected object is not the world object!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);

			return;
		}
	}

	//Deselect (click has been on empty space).
	public override void OnDeselect ()
	{
		base.OnDeselect ();

		if(fieldSkillActive || targetSkillActive)
		{
			targetSkillActive = false;
			fieldSkillActive = false;
			return;
		}
		else
		{
			PlayerManager.Instance.controllableObject = null;
			UIManager.Instance.OnObjectDeselected();
			SpotSelectController.Instance.OnDeselect();
			TargetMarkerController.Instance.OnDeselect();

			Debug.LogFormat ("{0} - {1}: deselect controllable object!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);
		}
	}

	//Set target (click has been on enemy).
	protected override void OnSetTarget (RaycastHit hit)
	{
		base.OnSetTarget (hit);

		WorldObject selectedObject = null;

		try
		{
			selectedObject = hit.transform.GetComponent<WorldObject> ();

			if(selectedObject.group != PlayerManager.Instance.playerGroup && selectedObject.group != 0)
			{
				PlayerManager.Instance.controllableObject.SetTarget(selectedObject);
				
				Debug.LogFormat ("{0} - {1}: selected new target - {2}!",
				                        DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name,
				                        selectedObject.name);
			}
			else
			{
				Debug.LogWarningFormat ("{0} - {1}: you cant attack friendly or neutral units!",
				                        DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name);
			}
		}
		catch (Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: selected target is not world object!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);
			
			return;
		}

		try
		{
			if(selectedObject.group != PlayerManager.Instance.playerGroup && selectedObject.group != 0)
			{
				PlayerManager.Instance.controllableObject.SetTarget(selectedObject);
			}
		}
		catch (Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: controllable object is not selected!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);
			
			return;
		}
	}

	//Set destination(click has been on ground rmb - 19th layaer).
	protected override void OnSetDestination (RaycastHit hit)
	{
		base.OnSetDestination (hit);

		try
		{
			PlayerManager.Instance.controllableObject.UnsetTarget();
			PlayerManager.Instance.controllableObject.SetDestination(hit.point);

			Debug.LogFormat ("{0} - {1}: set new destination - {2}",
			                 DateTime.Now.ToString ("HH:mm:ss:fff"),
			                 gameObject.name,
			                 hit.point);
		}
		catch (Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: controllable object is not selected!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);
			
			return;
		}
	}

	//Have camera control corners.
	protected override void OnScreenCorners (Vector3 direction)
	{
		base.OnScreenCorners (direction);

		Vector3 newDirrection = new Vector3(direction.z, direction.y, -direction.x);

		GameCameraController.Instance.SetCameraDirection (newDirrection);
	}

	//Activate skill.
	public void ActivateSkill(int slotId)
	{
		if(PlayerManager.Instance.player.GetSkillCooldownTimeNormalized(slotId) == 1f)
		{
			skillIdActive = slotId;

			switch(PlayerManager.Instance.player.skills[slotId].skillType)
			{
			case Skill.SkillType.target:
				targetSkillActive = true;
				break;
			case Skill.SkillType.field:
				fieldSkillActive = true;
				break;
			}
		}
	}

	//On select target for skill.
	protected override void OnSelectTargetForSkill(RaycastHit hit)
	{
		switch(PlayerManager.Instance.player.skills[skillIdActive].skillType)
		{
		case Skill.SkillType.target:

			WorldObject hitObject = hit.collider.GetComponent<WorldObject> ();
			
			//Filter.
			switch(PlayerManager.Instance.player.skills[skillIdActive].skillActivationType)
			{
			case Skill.SkillActivationType.onlyEnemy:
				if(hitObject.group == PlayerManager.Instance.playerGroup || !hitObject.isVisibleForAI)
					return;
				break;
			case Skill.SkillActivationType.onlyFriends:
				if(hitObject.group != PlayerManager.Instance.playerGroup || !hitObject.isVisibleForAI)
					return;
				break;
			}

			PlayerManager.Instance.player.ActivateSkill (skillIdActive, hitObject);

			break;
		case Skill.SkillType.field:
			PlayerManager.Instance.player.ActivateSkill (skillIdActive, new SkillAttackPoint(hit.point));
			break;
		}
	}

	//Listen keyboard input
	private void ListenKeyboarInput ()
	{
		//Escape.
		if (Input.GetButtonDown ("Cancel"))
		{
			if(fieldSkillActive || targetSkillActive)
			{
				targetSkillActive = false;
				fieldSkillActive = false;
			}
			else
			{
				OnDeselect ();
			}
		}

		//Now only 4 skills.
		if (Input.GetButtonDown ("UseSkillSlot_1"))
		{
			if(PlayerManager.Instance.player.skills[0].level > -1)
				ActivateSkill(0);
		}
		if (Input.GetButtonDown ("UseSkillSlot_2"))
		{
			if(PlayerManager.Instance.player.skills[1].level > -1)
				ActivateSkill(1);
		}
		if (Input.GetButtonDown ("UseSkillSlot_3"))
		{
			if(PlayerManager.Instance.player.skills[2].level > -1)
				ActivateSkill(2);
		}
		if (Input.GetButtonDown ("UseSkillSlot_4"))
		{
			if(PlayerManager.Instance.player.skills[3].level > -1)
				ActivateSkill(3);
		}
	}

	void Update()
	{
		CalculateScreenCorners ();
		if(SpotSelectController.Instance.getSelectedObject != null) // НЕ ЗАБЫТЬ ИСПРАВИТЬ ЭТОТ КОСТЫЛЬ!
		if(SpotSelectController.Instance.getSelectedObject.name == "Hero") // НЕ ЗАБЫТЬ ИСПРАВИТЬ ЭТОТ КОСТЫЛЬ!
			ListenKeyboarInput ();
	}
}