﻿using UnityEngine;
using System.Collections;

//Camera controller.
public class GameCameraController : MonoBehaviour
{
	//Camera modes.
	public enum CameraMode
	{
		free,
		assignToPlayer
	};

	//Singleton implementation.
	public static GameCameraController Instance { get; private set; }

	//Current camera mode.
	public CameraMode mode = CameraMode.free;

	//Random verctor.
	private Vector3 randomVector;

	public CameraMode currentMode
	{
		get
		{
			return mode;
		}
		set
		{
			if(mode != value)
			{
				mode = value;
				destination = transform.position;
				destination.y = 13f;
			}
		}
	}

	//Camera move speed;
	public float speed;

	//Camera move smoth.
	public float smooth;

	//Camera destination.
	public Vector3 destination;

	//Instance initialization.
	public void Awake()
	{
		if (Instance == null)
			Instance = this;

		destination = transform.position;
		destination.y = 13f;
	}

	public void SetRandomPosition(Vector3 pos)
	{
		randomVector = pos;
	}

	public void SetCameraDirection(Vector3 direction)
	{
		if(currentMode == CameraMode.free)
			destination += direction * Time.deltaTime * speed;
	}

	public void UpdateCameraTransfrom()
	{
		switch(currentMode)
		{
		case CameraMode.assignToPlayer:
			Vector3 cameraPosition = PlayerManager.Instance.player.transform.position + Vector3.left * 7f;
			cameraPosition.y = 13f;
			destination = cameraPosition;
			break;
		case CameraMode.free:
			break;
		}

		transform.position = Vector3.Lerp (transform.position + randomVector, destination, smooth * Time.deltaTime);
	}

	public void LateUpdate()
	{
		UpdateCameraTransfrom ();
	}
}