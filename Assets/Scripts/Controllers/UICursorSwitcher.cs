﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//Cursor controller.
public class UICursorSwitcher : MonoBehaviour
{
	public Sprite normalCursor, combatCursor;
	public Color normalColor, combatColor;
	
	private RectTransform rTransfrom;
	private Image currentImage;

	public void Start()
	{
		rTransfrom = transform as RectTransform;
		currentImage = GetComponent<Image> ();
	}

	public void LateUpdate()
	{
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = false;

		if(GameInputController.Instance.targetSkillActive)
		{
			rTransfrom.pivot = new Vector2(0.5f, 0.5f);
			currentImage.sprite = combatCursor;
			currentImage.color = combatColor;
		}
		else if(GameInputController.Instance.fieldSkillActive)
		{
			currentImage.color = Color.clear;
		}
		else
		{
			rTransfrom.pivot = new Vector2(0.06284122f, 0.9412068f);
			currentImage.sprite = normalCursor;
			currentImage.color = normalColor;
		}

		Vector3 cursorPosition = Input.mousePosition;

		rTransfrom.position = cursorPosition;
	}
}