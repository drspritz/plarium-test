﻿using UnityEngine;
using System;
using System.Collections;

//Simple ai for unit.
public class UnitAI : MonoBehaviour, ITriggerHandler
{
	//Current world object.
	public Unit controllableObject;

	//Current target.
	private WorldObject currentTarget;

	//Max distance to target.
	private float maxDist;

	//Initialization.
	public void Init()
	{
		maxDist = Mathf.Infinity;
	}

	//Call if any objects in agression range.
	public void OnCustomTriggerStay(Collider collider)
	{
		switch(controllableObject.currentMode)
		{
		case Unit.UnitMode.attack:
			//Do something for attack mode.
			break;
		case Unit.UnitMode.backToBase:
			CheckTargets(collider);
			break;
		case Unit.UnitMode.moveToDestination:
			//Do something for moveToDestination mode.
			break;
		case Unit.UnitMode.wait:
			CheckTargets(collider);
			break;
		}
	}

	//Check targets.
	private void CheckTargets(Collider collider)
	{
		float currentMaxDist = Vector3.Distance(transform.position, collider.transform.position);
		
		if(currentMaxDist < maxDist && collider.gameObject.layer == 20)
		{
			try
			{
				WorldObject selectedTarget = collider.GetComponent<WorldObject>();
				
				if(selectedTarget.group != 0 && selectedTarget.group != controllableObject.group && selectedTarget.isVisibleForAI)
				{
					currentTarget = selectedTarget;
					maxDist = currentMaxDist;
				}
			}
			catch (Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: target - {2} is not a world object!",
				                        DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name,
				                        collider.name);
			}
		}
	}

	//Clear all targets.
	public void ClearTargets()
	{
		controllableObject.UnsetTarget ();
		currentTarget = null;
	}

	//Update AI logic.
	public void UpdateAI()
	{
		switch(controllableObject.currentMode)
		{
		case Unit.UnitMode.attack:
			//Do something for attack mode.
			break;
		case Unit.UnitMode.backToBase:
			//Do something for backToBase mode.
			break;
		case Unit.UnitMode.moveToDestination:
			if(currentTarget != null)
			{
				if(currentTarget.group == 0) //Target was killed or somthin else.
				{
					ClearTargets();
				}
			}
			break;
		case Unit.UnitMode.wait:
			if(currentTarget != null)
			{
				controllableObject.SetTarget(currentTarget);
				maxDist = Mathf.Infinity;
			}
			else
			{
				controllableObject.BackToBase();
			}
			break;
		}
	}

	public void Start()
	{
		Init ();
	}

	public void LateUpdate()
	{
		UpdateAI ();
	}
}