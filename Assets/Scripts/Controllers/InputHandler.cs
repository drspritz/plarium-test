﻿using UnityEngine;
using System;
using UnityEngine.EventSystems;
using System.Collections;

//Capability with Unity 4.6.x.
//Handle mouse data. This script must be attached to Image into canvas, also in scene must be created event system.
public class InputHandler : MonoBehaviour, IPointerDownHandler
{
	//Field skill is active.
	[HideInInspector]
	public bool fieldSkillActive;
	//Target skill is active.
	[HideInInspector]
	public bool targetSkillActive;
	//Skill id.
	[HideInInspector]
	public int skillIdActive;
	
	//Mouse down.
	public void OnPointerDown (PointerEventData eventData)
	{
		Ray ray = new Ray();

		try
		{
			ray = Camera.main.ScreenPointToRay(new Vector3(eventData.position.x, eventData.position.y, 0f));
		}
		catch (Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: not have main camera for handle input data!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);
			
			return;
		}

		RaycastHit hit = new RaycastHit();

		if(Input.GetButton("Select"))
		{	
			if(Physics.Raycast(ray, out hit, Camera.main.farClipPlane, 1 << 19)) //Raycast only ground (19th layer).
			{
				if(fieldSkillActive)
				{
					OnSelectTargetForSkill(hit);
					targetSkillActive = false;
					fieldSkillActive = false;
					return;
				}
			}

			if(Physics.Raycast(ray, out hit, Camera.main.farClipPlane, 1 << 20)) //Raycast only world objecs (20th layer).
			{
				if(targetSkillActive)
				{
					OnSelectTargetForSkill(hit);
					targetSkillActive = false;
					fieldSkillActive = false;
					return;
				}
				else
				{
					OnSelect(hit);
					targetSkillActive = false;
					fieldSkillActive = false;
					return;
				}
			}
			else
			{
				OnDeselect();
				targetSkillActive = false;
				fieldSkillActive = false;
				return;
			}
		}

		if(Input.GetButton("Move"))
		{
			if(Physics.Raycast(ray, out hit, Camera.main.farClipPlane, 1 << 20)) //Raycast only world objecs (19th layer).
			{
				OnSetTarget(hit);
				targetSkillActive = false;
				fieldSkillActive = false;
				return;
			}

			if(Physics.Raycast(ray, out hit, Camera.main.farClipPlane, 1 << 19)) //Raycast only ground (19th layer).
			{
				OnSetDestination(hit);
				targetSkillActive = false;
				fieldSkillActive = false;
				return;
			}
		}
	}

	//Calculate cursor position (dont forget call this method every frame or when mouse move).
	public void CalculateScreenCorners()
	{
		Vector3 direction = Vector3.zero;

		if (Input.mousePosition.x <= 1)
			direction += Vector3.left;
		if (Input.mousePosition.x >= Screen.width - 1)
			direction +=  Vector3.right;
		if (Input.mousePosition.y <= 1)
			direction += Vector3.back;
		if (Input.mousePosition.y >= Screen.height - 1)
			direction += Vector3.forward;

		OnScreenCorners (direction.normalized);
	}

	//Select some object (click has been on world object 20th layaer).
	protected virtual void OnSelect (RaycastHit hit) {}
	//Select target for skill.
	protected virtual void OnSelectTargetForSkill(RaycastHit hit) {}
	//Deselect (click has been on empty space).
	public virtual void OnDeselect () {}
	//Set target (click has been on enemy).
	protected virtual void OnSetTarget (RaycastHit hit) {}
	//Set destination(click has been on ground rmb - 19th layaer).
	protected virtual void OnSetDestination (RaycastHit hit) {}
	//Have camera control corners.
	protected virtual void OnScreenCorners(Vector3 direction) {}

}