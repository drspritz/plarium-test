﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//UI manager handle all ui elements data.
public class UIManager : MonoBehaviour
{
	//Singleton implementation.
	public static UIManager Instance { get; private set; }

	//Action button template.
	public UISkillButton skillButtonTemplate;
	
	//Action button template.
	public UIActionButton barrackButtonTemplate;

	//Actions container.
	public RectTransform buttonsContainer;

	//Object info text.
	public Text objectInfo;
	private WorldObject selectedTarget;

	//Golds text.
	public Text goldText;

	//Respawn time text.
	public Text respawnTimeText;

	//Curren animator.
	public Animator messageAnim;
	
	//Current message labe.
	public Text messageLlabel;

	//Instance initialization.
	public void Awake()
	{
		if (Instance == null)
			Instance = this;
	}

	//Show player message.
	public void ShowPlayerMessage(string message)
	{
		messageLlabel.text = message;
		messageAnim.Play ("show_message", 0, 0f);
	}

	//On gold update.
	public void OnGoldUpdate(int currentGold)
	{
		goldText.text = string.Format ("Gold: {0}", currentGold);
	}

	//On object selected.
	public void OnObjectSelected(WorldObject selectedObject)
	{
		selectedTarget = selectedObject;
		ClearButtonsContainer ();
		CreateButtons ();
	}

	//On object selected.
	public void OnObjectDeselected()
	{
		selectedTarget = null;
		ClearButtonsContainer ();
	}

	//Create buttons.
	public void CreateButtons()
	{
		switch(selectedTarget.GetType().Name)
		{
		case "Barrack":
			UIActionButton actionButton = Instantiate(barrackButtonTemplate) as UIActionButton;
			actionButton.transform.SetParent(buttonsContainer);
			actionButton.name = "BarrackUpdateButton";
			actionButton.buttonLabel.text = "Upgrade";
			actionButton.messageReceiver = selectedTarget;
			break;
		case "Hero":
			Hero hero = selectedTarget as Hero;
			int skillsCount = hero.skills.Length;

			for(int i = 0; i < skillsCount; i++)
			{
				UISkillButton skillButton = Instantiate(skillButtonTemplate) as UISkillButton;
				skillButton.transform.SetParent(buttonsContainer);
				skillButton.name = string.Format("SkillButton_{0}", i + 1);
				skillButton.hero = hero;
				skillButton.slotId = i;
			}
			break;
		}
	}

	//Delete all buttons in container.
	public void ClearButtonsContainer()
	{
		int buttonsCount = buttonsContainer.childCount;

		for(int i = 0; i < buttonsCount; i++)
		{
			Destroy(buttonsContainer.GetChild(i).gameObject);
		}
	}

	public void Update()
	{
		if(selectedTarget != null)
		{
			if(selectedTarget.group == 0)
			{
				selectedTarget = null;
				return;
			}

			objectInfo.text = selectedTarget.ToString ();
		}
		else
		{
			objectInfo.text = "Not selected";
		}

		if(PlayerManager.Instance.playerRespawnTimer == 0f)
		{
			respawnTimeText.text = "Player is alive!!1";
		}
		else
		{
			respawnTimeText.text = string.Format("Player respawn time: {0}", System.Math.Round(PlayerManager.Instance.playerRespawnTimer, 2));
		}
	}
}