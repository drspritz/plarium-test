﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//UI message manager.
public class UIFadeController : MonoBehaviour
{
	//Singleton implementation.
	public static UIFadeController Instance { get; private set; }

	//Curren animator.
	public Animator anim;

	//Instance initialization.
	public void Awake()
	{
		if (Instance == null)
			Instance = this;
	}

	//Fade in.
	public void FadeIn()
	{
		anim.Play ("fadeIn", 0, 0f);
	}

	//Fade out.
	public void FadeOut()
	{
		anim.Play ("fadeOut", 0, 0f);
	}
}