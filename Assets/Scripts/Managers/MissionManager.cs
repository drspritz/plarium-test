﻿using UnityEngine;
using System;
using System.Collections;

//Mission manager.
public class MissionManager : MonoBehaviour
{
	//Mission mode types.
	public enum MissionMode
	{
		pause,
		waves
	};

	//Singleton.
	public static MissionManager Instance { get; private set; }

	//Current mission mode.
	public MissionMode currentMode;

	//Pause long time.
	public float pauseTime;

	//Wave long time.
	public float waveTime;
	
	//Enemy barracks in scene.
	public Barrack[] enemyBarracks;
	//All active buildings.
	public Building[] otherBuildings;

	//Base position.
	public Transform unitBase;

	//Instance initialization.
	public void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
			Init();
		}
	}

	public void Start()
	{
		StartCoroutine (WaveProcess());
	}

	//Class initialization.
	public void Init()
	{
		Base.BaseDestroyingEvent += OnPlayerBaseDestroy;
	}

	public Vector3 GetBasePosition(WorldObject obj)
	{
		try
		{
			if (obj.group == PlayerManager.Instance.playerGroup)
			{
				return unitBase.position;
			}
			else
			{
				return Vector3.zero;
			}
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: PlayerManager instance not initialized!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
	
			return Vector3.zero;
		}
	}

	//On player base destroy.
	public void OnPlayerBaseDestroy(int baseGroup)
	{
		try
		{
			if(PlayerManager.Instance.playerGroup == baseGroup)
				Debug.LogWarning ("PLAYER BASE DESTROED!");

			StopAllCoroutines();
			StartCoroutine(GameOver("Your base DESTROYED!\nGAME OVER!"));
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: PlayerManager instance not initialized!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);

			return;
		}
	}

	//Game over =(
	public IEnumerator GameOver(string message)
	{
		UIFadeController.Instance.FadeIn ();

		yield return new WaitForSeconds (0.5f);

		UIManager.Instance.ShowPlayerMessage(string.Format(message, pauseTime));

		yield return new WaitForSeconds (5f);

		Application.LoadLevel ("Menu");
	}

	//Upgrade barracks.
	private void UpgradeEnemyBarracks()
	{
		foreach(Barrack barrack in enemyBarracks)
		{
			barrack.LevelUp();
		}
	}

	//Enable barracks.
	private void EnableBarracks(bool barrackEnable)
	{
		foreach(Barrack barrack in enemyBarracks)
		{
			barrack.enabled = barrackEnable;
		}

		foreach(Barrack barrack in otherBuildings)
		{
			barrack.enabled = barrackEnable;
		}
	}

	//Wave process.
	public IEnumerator WaveProcess()
	{
		int waveCount = 0;

		EnableBarracks(false);

		yield return new WaitForSeconds(2f);

		UIManager.Instance.ShowPlayerMessage(string.Format("You must protect the base of blue rabbits from hordes of purpple bears!\nFight bravely, my friend!", pauseTime));

		yield return new WaitForSeconds(5f);

		while(waveCount < 10)
		{
			waveCount++;

			EnableBarracks(false);

			UIManager.Instance.ShowPlayerMessage(string.Format("{0} seconds for prepare to battle!", pauseTime));

			yield return new WaitForSeconds(pauseTime);

			UIManager.Instance.ShowPlayerMessage(string.Format("Wave {0}!\nProtect the base for {1} seconds", waveCount, waveTime));

			UpgradeEnemyBarracks();
			EnableBarracks(true);
				
			yield return new WaitForSeconds(waveTime);
		}

		StopAllCoroutines();
		StartCoroutine(GameOver("You are won!\nBlue bunnies will never forget your help!"));
	}
}