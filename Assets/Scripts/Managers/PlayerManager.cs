﻿using UnityEngine;
using System.Collections;

//Player manager.
public class PlayerManager : MonoBehaviour
{
	//Singleton implementation.
	public static PlayerManager Instance { get; private set; }

	//Player respawn point.
	public Transform playerRespawnPoint;

	//How much gold have player at start the game.
	public int initialPlayerGold;

	//How far should be a player to get experience.
	public float distanceForGetExp;

	//Hero.
	public Hero player;

	//Current controllable object.
	public IControllable controllableObject;

	//What group has the player?
	public int playerGroup;

	//Gold count.
	public int gold { get; private set; }

	//Player respawn timer.
	public float playerRespawnTimer { get; private set; }

	//Instance initialization.
	public void Awake()
	{
		if (Instance == null)
			Instance = this;

		Unit.UnitDestroyEvent += OnUnitDestroyEventHandler;
	}

	//Registrate player.
	public void RegistratePlayerHero(Hero hero)
	{
		if (player == null)
		{
			player = hero;
			GoldAdd(10);
		}
	}

	//Handle units death and get epx.
	public void OnUnitDestroyEventHandler(Unit unit)
	{
		float currentDistance = Vector3.Distance (player.transform.position, unit.transform.position);

		if(currentDistance < distanceForGetExp)
		{
			player.OnGetExp(unit.exp);
		}
	}

	//Transaction - subtract.
	public bool GoldSubtract(int count)
	{
		int inputCount = Mathf.Abs(count);
		int goldBalance = gold - inputCount;

		if(goldBalance >= 0)
		{
			gold = goldBalance;
			UIManager.Instance.OnGoldUpdate (gold);
			return true;
		}
		else
		{
			return false;
		}
	}

	//Transaction - add.
	public void GoldAdd(int count)
	{
		int inputCount = Mathf.Abs(count);
		gold += inputCount;

		UIManager.Instance.OnGoldUpdate (gold);
	}

	//On player is dead.
	public void OnPlayerDead()
	{
		if (player.group == 0)
		{
			StartCoroutine (PlayerRespawnProcess(player.respawn));

			try
			{
				GameCameraController.Instance.currentMode = GameCameraController.CameraMode.free;
			}
			catch(System.Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: GameCameraController instance not initialized!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        gameObject.name);
				
				enabled = false;
				return;
			}
		}
	}

	//Respawn process.
	private IEnumerator PlayerRespawnProcess(float respawnTime)
	{
		float startTime = Time.time;
		float respawnEndTime = startTime + respawnTime;
		
		while(respawnEndTime > Time.time)
		{
			playerRespawnTimer = respawnEndTime - Time.time;
			yield return null;
		}

		playerRespawnTimer = 0f;

		PlayerRespawn ();
	}

	//Respawn player.
	private void PlayerRespawn()
	{
		player.transform.position = playerRespawnPoint.position;
		player.Resurrection ();

		Vector3 cameraPosition = player.transform.position + Vector3.left * 7f;
		cameraPosition.y = 13f;

		try
		{
			GameCameraController.Instance.destination = cameraPosition;
		}
		catch(System.Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: GameCameraController instance not initialized!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
			
			enabled = false;
			return;
		}
}
}