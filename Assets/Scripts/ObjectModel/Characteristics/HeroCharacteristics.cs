﻿using UnityEngine;
using System.Collections;

[System.Serializable]
//HeroCharacteristics class contain information about hero.
public class HeroCharacteristics : UnitCharacteristics
{
	//How mutch time need for respawn hero.
	public float respawnTime;

	//How mutch experience need to new level.
	public int nextLevelExpCount;

	//Emty constructor.
	public HeroCharacteristics() {}
	
	//Constructor with data.
	public HeroCharacteristics(int _hp, float _armor, float _attack, float _attackSpeed, float _speed, float _attackRange, int _gold, int _exp, float _respawnTime, int _nextLevelExpCount)
	{
		respawnTime = _respawnTime;
		nextLevelExpCount = _nextLevelExpCount;
	}
}