﻿using UnityEngine;
using System.Collections;

[System.Serializable]
//BarrackCharacteristics class contain information about upgrade cost, training speed and how many boost will have unit, if building get a new level.
public class BarrackCharacteristics : UnitCharacteristics
{
	//Upgrade cost.
	public int upgradeCost;
	//Training speed.
	public float trainingSpeed;

	//Emty constructor.
	public BarrackCharacteristics() {}

	//Constructor with data.
	public BarrackCharacteristics(int _hp, float _armor, float _attack, float _attackSpeed, float _speed, float _attackRange, int _gold, int _exp, int _upgradeCost, float _trainingSpeed)
	{
		exp = _exp;
		upgradeCost = _upgradeCost;
		trainingSpeed = _trainingSpeed;
	}
}