﻿using UnityEngine;
using System.Collections;

[System.Serializable]
//WorldObjectCharacteristics class contain information about world object.
public class WorldObjectCharacteristics
{
	//Unit health.
	public int hp;
	//Unit armor.
	public float armor;

	//Emty constructor.
	public WorldObjectCharacteristics() {}

	//Constructor with data.
	public WorldObjectCharacteristics(int _hp, float _armor)
	{
		hp = _hp;
		armor = _armor;
	}
}