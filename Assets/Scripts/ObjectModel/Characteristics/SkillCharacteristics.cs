﻿using UnityEngine;
using System.Collections;

[System.Serializable]
//SkillCharacteristics class contain information about skill.
public class SkillCharacteristics
{
	//Skill cooldown.
	public float cooldown;
	
	//Skill power.
	public float power;

	//Skill cast speed.
	public float castSpeed;

	//Activation range.
	public float activationRange;
	
	//Power radius, it is not used for single targets like self and target skill tipes.
	public float powerRange;
	
	//Skill effect duration.
	public float effectDuration;
	
	//Skill effect.
	public UnitCharacteristics effect;

	//Emty constructor.
	public SkillCharacteristics() {}
	
	//Constructor with data.
	public SkillCharacteristics(float _cooldown, float _power, float _castSpeed, float _activationRange, float _powerRange, float _effectDuration, UnitCharacteristics _effect)
	{
		cooldown = _cooldown;
		power = _power;
		castSpeed = _castSpeed;
		activationRange = _activationRange;
		powerRange = _powerRange;
		effectDuration = _effectDuration;
		effect = _effect;
	}
}