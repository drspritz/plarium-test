﻿using UnityEngine;
using System.Collections;

[System.Serializable]
//UnitCharacteristics class contain information about unit.
public class UnitCharacteristics : WorldObjectCharacteristics
{
	//Unit attack power.
	public float attack;
	//Unit attack speed.
	public float attackSpeed;
	//Unit move speed.
	public float speed;
	//Unit attack range.
	public float attackRange;
	//Unit gold.
	public int gold;
	//Unit experience.
	public int exp;

	//Emty constructor.
	public UnitCharacteristics() {}

	//Constructor with data.
	public UnitCharacteristics(int _hp, float _armor, float _attack, float _attackSpeed, float _speed, float _attackRange, int _gold, int _exp)
	{
		attack = _attack;
		attackSpeed = _attackSpeed;
		speed = _speed;
		attackRange = _attackRange;
		gold = _gold;
		exp = _exp;
	}
}