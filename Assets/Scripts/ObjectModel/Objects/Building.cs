﻿/// В ТЗ не было задачи сделать возможность восстанавливать здания, но мне пришлось это сделать,
/// что бы не нарушать логику структуры классов.

using UnityEngine;
using System.Collections;

//The basic model of the building.
public class Building : WorldObject, IReparable
{
	//Repair building.
	public void Repair (float restoreValue, int repearGroup)
	{
		if(repearGroup == group)
		{
			hp += restoreValue;
		}
		else
		{
			//How the building to react, if the some unfriendly object is trying to repair them
			//damage, buff/debuff or something else.
		}
	}
}