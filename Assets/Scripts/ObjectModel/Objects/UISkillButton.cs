﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UISkillButton : MonoBehaviour
{
	public Text skillName;
	public Text cooldownLabel;
	public Text hotkeyLabel;
	public Text skillLevelLabel;
	public GameObject skillLevelUpButton;

	[HideInInspector]
	public Hero hero;
	[HideInInspector]
	public int slotId;

	public void LateUpdate()
	{
		if(hero != null)
		{
			skillName.text = hero.skills[slotId].name;
			hotkeyLabel.text = (slotId + 1).ToString();
			skillLevelLabel.text = string.Format("Level: {0}", hero.skills[slotId].level + 1);

			if(hero.skills[slotId].level > -1)
			{
				float colldownTime = hero.GetSkillCooldownTime(slotId);

				if(colldownTime != hero.skills[slotId].cooldown)
				{
					cooldownLabel.text = System.Math.Round(colldownTime, 2).ToString();
				}
				else
				{
					cooldownLabel.text = "Ready";
				}
			}
			else
			{
				cooldownLabel.text = "Up me!";
			}

			if(PlayerManager.Instance.player.skillPoint > 0)
			{
				skillLevelUpButton.SetActive(true);
			}
			else
			{
				skillLevelUpButton.SetActive(false);
			}
		}
	}

	//On button click.
	public void OnButtonActive()
	{
		if(hero.skills[slotId].level > -1)
			GameInputController.Instance.ActivateSkill (slotId);
	}

	public void OnSkillUp()
	{
		PlayerManager.Instance.player.SkillLevelUp (slotId);
	}
}