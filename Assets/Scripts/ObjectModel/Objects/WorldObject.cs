﻿using UnityEngine;
using System.Collections;

//The basic model of the object with which the player can interact.
public class WorldObject : MonoBehaviour, IDamageable
{
	//Is visible for AI?
	public bool isVisibleForAI;

	//Object friendly group, 0 - unknown or neutral group.
	public int initialGroup;

	//Current group.
	private int currentGroup;

	//Initial object HP.
	public float initialHp;
	//Initial object armor.
	public float initialArmor;

	//Max hp, which object may have.
	protected float maxHp;
	//Current HP.
	private float currentHp;
	//Current armor strength.
	private float currentArmor;

	//Sphere collider of the object.
	private SphereCollider sCollider;

	//For restore characteristics after effects.
	protected float oldMaxHp;
	protected float oldArmor;

	//Effect process coroutine.
	private Coroutine effctCoroutine;

	//HP getter and setter for public.
	public float hp
	{
		get
		{
			return currentHp;
		}
		protected set
		{
			if(currentHp != value)
			{
				currentHp = Mathf.Clamp(value, 0f, maxHp);
			}
		}
	}

	//Armor getter and setter for public.
	public float armor
	{
		get
		{
			return currentArmor;
		}
		protected set
		{
			if(currentArmor != value)
			{
				currentArmor = Mathf.Clamp(value, 0f, 1f);
			}
		}
	}

	//Group getter and setter for public.
	public int group
	{
		get
		{
			return currentGroup;
		}
		protected set
		{
			if(currentGroup != value)
			{
				currentGroup = value;
			}
		}
	}

	//Object initialization.
	public virtual void Init()
	{
		maxHp = initialHp;
		hp = maxHp;
		armor = initialArmor;
		group = initialGroup;

		try
		{
			sCollider = GetComponent<SphereCollider>();
			sCollider.isTrigger = true;
		}
		catch(System.Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			
			Debug.LogWarningFormat ("{0} - {1}: the object has not have a sphere collider!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);

			enabled = false;
			return;
		}
	}

	//Damage object. Reture true if this hit was last.
	public bool ApplyDamage (float damageValue, float delay)
	{
		float damageForApply = damageValue - armor * damageValue;
		hp -= damageForApply;

		OnDamageTaken (damageValue, damageForApply);

		if (hp == 0f)
		{
			DestroyObject (delay);
			return true;
		}
		else
		{
			return false;
		}
	}

	//Apply buffs or debuffs.
	public void ApplyEffect(SkillCharacteristics skillCharacteristics)
	{
		if (effctCoroutine != null)
		{
			StopCoroutine ("ApplyEffectProcess");
			RestoreCharacteristics ();
		}

		effctCoroutine = StartCoroutine ("ApplyEffectProcess", skillCharacteristics);
	}

	//Remember current characteristics.
	protected virtual void RememberCharacteristics()
	{
		oldMaxHp = maxHp;
		oldArmor = armor;
	}

	//Restore old characteristics.
	protected virtual void RestoreCharacteristics()
	{
		maxHp = oldMaxHp;
		armor = oldArmor;
	}

	//Apply effect IEnumerator.
	protected virtual IEnumerator ApplyEffectProcess(SkillCharacteristics skillCharacteristics)
	{
		yield return new WaitForSeconds (skillCharacteristics.castSpeed);

		//Apply effect
		maxHp += skillCharacteristics.effect.hp;
		armor += skillCharacteristics.effect.armor;

		//Clamp HP for maxHp.
		hp = Mathf.Clamp(hp, 0f, maxHp);

		//Wait until effect works.
		yield return new WaitForSeconds (skillCharacteristics.effectDuration);

		RestoreCharacteristics ();
	}

	//Call before damage taken.
	protected virtual void OnDamageTaken (float damageValue, float lossesHp)
	{
		Debug.LogFormat ("{0} - {1}: the object has received {2} damage and lose {3} HP!",
		                 System.DateTime.Now.ToString("HH:mm:ss:fff"),
		                 gameObject.name,
		                 damageValue,
		                 lossesHp);
	}

	//Call if object was destroyed.
	protected virtual void DestroyObject (float delay)
	{
		Debug.LogFormat ("{0} - {1}: the object was destroyed!",
		                 System.DateTime.Now.ToString("HH:mm:ss:fff"),
		                 gameObject.name);
		group = 0;
	}

	//Upgrade object.
	protected virtual void OnUnitUpgrade(int newLevel, WorldObjectCharacteristics characteristcs)
	{
		//Stop work current effect.
		StopCoroutine ("ApplyEffectProcess");

		maxHp = initialHp + characteristcs.hp;
		armor = initialArmor + characteristcs.armor;

		hp = maxHp;

		RememberCharacteristics ();
	}

	//Get class info.
	public override string ToString ()
	{
		return string.Format ("Name: {0}\n" +
		                      "HP: {1}\n" +
		                      "Armor: {2}\n",
		                      name,
		                      hp,
		                      armor);
	}
}