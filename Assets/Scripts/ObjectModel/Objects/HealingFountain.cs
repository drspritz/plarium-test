﻿using UnityEngine;
using System;
using System.Collections;

//Fountain of the life energy, which heal friendly minions and characters (heroes).
public class HealingFountain : Building, ITriggerHandler
{
	//How much HP will be restored to character per second.
	public float characterHeal;
	//How much HP will be restored to unit per second.
	public float unitHeal;

	//Call every frame if any objects is entered in collider.
	public void OnCustomTriggerStay(Collider other)
	{
		if(other.gameObject.layer == 20) // layer 20 contains only world objects that may interact with similar.
		{
			IHealed iHealed = other.GetComponent<IHealed>();

			if(other.tag == "Character")
			{
				if(iHealed != null)
					iHealed.Heal(characterHeal * Time.deltaTime, group);
			}
			else
			{
				if(iHealed != null)
					iHealed.Heal(unitHeal * Time.deltaTime, group);
			}
		}
	}

	void Start()
	{
		Init ();
	}
}