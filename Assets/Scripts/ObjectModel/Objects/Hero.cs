﻿using UnityEngine;
using System;
using System.Collections;

//Character class.
public class Hero : Unit
{
	//Initial respawn time.
	public float initialRespawnTime;

	//Initial next level exp count.
	public int initialNextLevelExpCount;

	//Current respawn time.
	private float currentRespawnTime;

	//Current next level exp count.
	private int currentNextLevelExpCount;

	//Character skills.
	public Skill[] skills;
	//Skills cooldwn.
	private float[] skillsCooldown;

	//Boosts for each level.
	public HeroCharacteristics[] heroLevels;

	//Current level.
	private int currentLevel;

	//Skill for use.
	public int skillSlotIdForUse { get; private set; }
	//Skill target.
	private System.Object skillTarget;
	
	//Level up effect !!!!!!REFERENCE TO EXIST OBJECT!!!!!
	public FX levelUpFX;

	//Exp count.
	public int currentExp { get; private set; }
	//Skill point count.
	public int skillPoint { get; private set; }

	//Respawn getter and setter for public.
	public float respawn
	{
		get
		{
			return currentRespawnTime;
		}
		protected set
		{
			if(currentRespawnTime != value)
			{
				currentRespawnTime = Mathf.Clamp(value, 0f, 99999f);
			}
		}
	}

	//Next level exp count getter and setter for public.
	public int nextLevelExpCount
	{
		get
		{
			return currentNextLevelExpCount;
		}
		protected set
		{
			if(currentNextLevelExpCount != value)
			{
				currentNextLevelExpCount = Mathf.Clamp(value, 0, 99999);
			}
		}
	}

	//Level getter and setter for public.
	public int level
	{
		get
		{
			return currentLevel;
		}
		protected set
		{
			if(currentLevel != value)
			{
				currentLevel = Mathf.Clamp(value, 0, heroLevels.Length);
			}
		}
	}

	//Initialization.
	public override void Init ()
	{
		base.Init ();
	
		try
		{
			group = PlayerManager.Instance.playerGroup;
			PlayerManager.Instance.RegistratePlayerHero (this);
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: PlayerManager instance not initialized!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
			
			enabled = false;
			return;
		}

		respawn = initialRespawnTime;
		skillsCooldown = new float[skills.Length];

		for(int i = 0; i < skillsCooldown.Length; i++)
		{
			skillsCooldown[i] = 1f;
		}

		StartCoroutine (DelayedLevelUp(1f));
	}

	//Get skill cooldown time.
	public float GetSkillCooldownTime(int skillSlotId)
	{
		return skillsCooldown [skillSlotId] * skills [skillSlotId].cooldown;
	}
	//Get skill cooldown time normalized.
	public float GetSkillCooldownTimeNormalized(int skillSlotId)
	{
		return skillsCooldown [skillSlotId];
	}

	//Handle units death and get epx.
	public void OnGetExp(int expCount)
	{
		currentExp += expCount;
		if(currentExp >= nextLevelExpCount)
			LevelUp();
	}

	//Level up.
	private void LevelUp()
	{
		if(level < heroLevels.Length)
		{
			level++;
			OnUnitUpgrade (level - 1, heroLevels [level - 1]);

			Debug.LogFormat ("{0} - {1}: successfuly upgraded to next level - {2}!",
			                 DateTime.Now.ToString ("HH:mm:ss:fff"),
			                 gameObject.name, level);
		}
		else
		{
			Debug.LogWarningFormat ("{0} - {1}: have max level!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
		}
	}

	//On kill somthing.
	public override void OnKill (WorldObject killedObject)
	{
		base.OnKill (killedObject);

		Unit killedUnit = null;

		try
		{
			killedUnit = killedObject as Unit;
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: killed object is not a unit!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);

			return;
		}

		try
		{
			PlayerManager.Instance.GoldAdd(killedUnit.gold);
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: PlayerManager instance not initialized!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
			
			enabled = false;
			return;
		}
	}

	//Hero level up.
	protected override void OnUnitUpgrade (int newLevel, WorldObjectCharacteristics characteristcs)
	{
		base.OnUnitUpgrade (newLevel, characteristcs);

		HeroCharacteristics inputCharacteristics = new HeroCharacteristics ();

		try
		{
			inputCharacteristics = characteristcs as HeroCharacteristics;
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: input characteristics is not a HeroCharacteristics!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
			
			enabled = false;
			return;
		}

		respawn = initialRespawnTime + inputCharacteristics.respawnTime;
		nextLevelExpCount = initialNextLevelExpCount + inputCharacteristics.nextLevelExpCount;

		skillPoint++;

		levelUpFX.PlayFX (0f);
	}

	//Activate skill.
	public void ActivateSkill(int skillSlotId, System.Object target)
	{
		if(skillsCooldown[skillSlotId] >= 1f)
		{
			skillSlotIdForUse = skillSlotId;
			skillTarget = target;
		}
		else
		{
			Debug.LogWarningFormat ("{0} - {1}: {2} is not ready yet !",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name,
			                        skills [skillSlotId].name);
		}
	}

	//Extend SetTarget.
	public override void SetTarget (WorldObject target)
	{
		base.SetTarget (target);
	
		skillTarget = null;
	}

	//Extend UnsetTarget.
	public override void UnsetTarget ()
	{
		base.UnsetTarget ();

		skillTarget = null;
	}

	//Cooldown process.
	private IEnumerator ColldownProcess(int skillSlotId)
	{
		float startTime = Time.time;
		float colldownEndTime = startTime + skills[skillSlotId].cooldown;

		while(colldownEndTime > Time.time)
		{
			skillsCooldown[skillSlotId] = (colldownEndTime - Time.time) / skills[skillSlotId].cooldown;
			yield return null;
		}

		skillsCooldown [skillSlotId] = 1f;
	}

	//Override UnitProcess.
	protected override void UnitProcess ()
	{
		if(skillTarget != null)
		{
			float distance = 999999f;

			try
			{
				if(skillTarget.GetType() == typeof(SkillAttackPoint))
				{
					distance = Vector3.Distance(transform.position, (skillTarget as SkillAttackPoint).attackPoint);
				}
				else
				{
					distance = Vector3.Distance(transform.position, (skillTarget as WorldObject).transform.position);
				}
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: target is not a world object and is not a position!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        gameObject.name);
				
				currentMode = UnitMode.wait;
				return;
			}
			
			if(distance < skills [skillSlotIdForUse].activationRange)
			{
				try
				{
					skills [skillSlotIdForUse].killDelegate = OnKill;
					skills [skillSlotIdForUse].Use(skillTarget);

					StartCoroutine(ColldownProcess(skillSlotIdForUse));

					skillTarget = null;
				}
				catch(Exception ex)
				{
					Debug.LogWarningFormat (ex.Message);
					Debug.LogWarningFormat ("{0} - {1}: target is not a world object!",
					                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
					                        gameObject.name);
					
					currentMode = UnitMode.wait;
					return;
				}
			}
			else
			{
				if(skillTarget.GetType() == typeof(SkillAttackPoint))
				{
					SetDestination((skillTarget as SkillAttackPoint).attackPoint);
				}
				else
				{
					SetDestination((skillTarget as WorldObject).transform.position);
				}
			}
		}
		else
		{
			base.UnitProcess ();
		}
	}

	//Try up skill.
	public void SkillLevelUp(int skillSlotId)
	{
		if (skills [skillSlotId].LevelUp ())
			skillPoint--;
	}

	//Destroy hero.
	protected override void DestroyObject (float delay)
	{
		try
		{
			FX fx = Instantiate (deathFx, transform.position, Quaternion.Euler(new Vector3(-90f, 0f, 0f))) as FX;
			fx.PlayFX (delay);
		}
		catch(System.Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: deathFx not defined!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
			
			return;
		}


		Debug.LogFormat ("{0} - {1}: the object was destroyed!",
		                 System.DateTime.Now.ToString("HH:mm:ss:fff"),
		                 gameObject.name);
		group = 0;

		UnsetTarget ();
		SetDestination (PlayerManager.Instance.playerRespawnPoint.transform.position);

		gameObject.SetActive (false);

		PlayerManager.Instance.OnPlayerDead ();
	}

	//Hero resurrection
	public void Resurrection()
	{
		Heal (Mathf.Infinity, group);
		group = PlayerManager.Instance.playerGroup;
		gameObject.SetActive (true);

		for(int i = 0; i < skillsCooldown.Length; i++)
		{
			skillsCooldown[i] = 1f;
		}
	}

	//Get class info.
	public override string ToString ()
	{
		string oldString = base.ToString ();

		return string.Format (oldString + 
		                      "Level: {0}\n" +
		                      "CurrentExpCount: {1}\n" +
		                      "NextLevelExpCount: {2}\n" +
		                      "SkillPoints: {3}\n",
		                      currentLevel,
		                      currentExp,
		                      nextLevelExpCount,
		                      skillPoint);
	}

	public IEnumerator DelayedLevelUp(float delay)
	{
		yield return new WaitForSeconds (delay);

		LevelUp ();
	}

	public void Start()
	{
		Init ();
	}

	public void Update()
	{
		UnitProcess ();
	}
}