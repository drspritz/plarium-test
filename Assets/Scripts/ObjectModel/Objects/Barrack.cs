using UnityEngine;
using System;
using System.Collections;

//Barrack, which make a new units.
[RequireComponent(typeof(SphereCollider))]
public class Barrack : Building
{
	//Barrack level up delegate.
	public delegate void BarrackLevelUpEventHandler (int newLevel, UnitCharacteristics levelBoost);
	//Barrack level up event.
	public event BarrackLevelUpEventHandler BarrackLevelUpEvent;

	//Spawned units group.
	public int unitsGroup;

	//How many units can be simultaneously from this barrack.
	public int maxUnitsCount;

	//Current unit count;
	private int currentUnitsCount;

	//Unit template which will make barrack.
	public Unit unitTemplate;

	//Positions, where units will be spawn randomly.
	public Transform[] spawningPoints;

	//Boosts for each level.
	public BarrackCharacteristics[] barrackLevels;

	//Current level of the barrack.
	private int currentLevel;

	//Spawn timer.
	private float spawnTimer;

	//Level getter and setter for public.
	public int level
	{
		get
		{
			return currentLevel;
		}
		protected set
		{
			if(currentLevel != value)
			{
				currentLevel = Mathf.Clamp(value, 0, barrackLevels.Length);
			}
		}
	}

	//Level getter and setter for public.
	public int unitsCount
	{
		get
		{
			return currentUnitsCount;
		}
		protected set
		{
			if(currentUnitsCount != value)
			{
				currentUnitsCount = Mathf.Clamp(value, 0, 99);
			}
		}
	}

	//Skill cooldown.
	public float upgradeCost
	{
		get
		{
			return barrackLevels[level - 1].upgradeCost;
		}
	}
	
	//Skill power.
	public float trainingSpeed
	{
		get
		{
			return barrackLevels[level - 1].trainingSpeed;
		}
	}

	//Barrack level up event caller.
	public void OnBarrackLevelUp(int newLevel, UnitCharacteristics levelBoost)
	{		
		if (BarrackLevelUpEvent != null)
		{
			BarrackLevelUpEvent(newLevel, levelBoost);
		}
	}
	
	//Override init method.
	public override void Init ()
	{
		base.Init ();

		spawnTimer = 0f;

		LevelUp ();
	}

	//Try up barrack level.
	public bool LevelUp()
	{
		int currentUpgradeCost = 0;

		try
		{
			if(level != 0)
			{
				currentUpgradeCost = barrackLevels [level - 1].upgradeCost;
			}
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: not defined level characteristics - {2}, barrack has not been upgraded!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);

			return false;
		}

		if (PlayerManager.Instance.GoldSubtract(currentUpgradeCost))
		{
			level++;
			OnBarrackLevelUp (level - 1, barrackLevels [level - 1]);

			Debug.LogFormat ("{0} - {1}: barrack was successfuly upgraded to next level - {2}!",
			                 DateTime.Now.ToString ("HH:mm:ss:fff"),
			                 gameObject.name, level);

			return true;
		}
		else
		{
			Debug.LogWarningFormat ("{0} - {1}: not enough gold, barrack has not been upgraded!",
				                    DateTime.Now.ToString ("HH:mm:ss:fff"),
				                    gameObject.name);

			return false;
		}
	}

	//Spawning.
	private void Spawn()
	{


		Vector3 unitPosition = Vector3.zero;
		Quaternion unitRotation = Quaternion.identity;

		try
		{
			unitPosition = spawningPoints[UnityEngine.Random.Range(0, spawningPoints.Length)].position;
			unitRotation = spawningPoints[UnityEngine.Random.Range(0, spawningPoints.Length)].rotation;
		}
		catch (Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: not defined any spawning points, barrack will be disabled!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);

			enabled = false;
			return;
		}

		UnitCharacteristics characteristics = new UnitCharacteristics ();

		try
		{
			characteristics = barrackLevels [level - 1] as UnitCharacteristics;
		}
		catch (Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: not defined level characteristics - {2}, barrak will be disabled!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name,
			                        level);
			
			enabled = false;
			return;
		}

		try
		{
			Unit spawnedUnit = Instantiate(unitTemplate, unitPosition, unitRotation) as Unit;

			spawnedUnit.initialGroup = unitsGroup;
			spawnedUnit.Init();
			spawnedUnit.Configure(characteristics, this);

			unitsCount++;
		}
		catch (Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: not defined unit template, barrak will be disabled!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name,
			                        level);
			
			enabled = false;
			return;
		}
	}

	//Update barrack making process.
	public void UpdateBarrack()
	{
		spawnTimer += Time.deltaTime;

		float currentTrainingSpeed = 0f;

		try
		{
			currentTrainingSpeed = barrackLevels[level - 1].trainingSpeed;
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: not defined level characteristics - {2}, barrack has been disabled!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name,
			                        level);
			
			enabled = false;
			return;
		}

		if(spawnTimer > currentTrainingSpeed && unitsCount < maxUnitsCount)
		{
			Spawn();
			spawnTimer = 0f;
		}
	}

	//On unit died.
	public void OnUnitDeath()
	{
		unitsCount--;
	}

	//Get class info.
	public override string ToString ()
	{
		string oldString = base.ToString ();
		
		return string.Format (oldString + 
		                      "Level: {0}\n" +
		                      "UpgradeCost: {1} gold\n" +
		                      "UnitType: {2}\n"+
		                      "TrainingSpeed: {3}",
		                      currentLevel,
		                      upgradeCost,
		                      unitTemplate.name,
		                      trainingSpeed);
	}

	public void Start()
	{
		Init ();
	}

	public void OnEnable()
	{
		spawnTimer = 0f;
	}

	public void Update()
	{
		UpdateBarrack ();

		if(Input.GetKeyDown(KeyCode.Space) && group == 1)
		{
			LevelUp();
		}
	}
}