﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//UI action button.
public class UIActionButton : MonoBehaviour
{
	public Text buttonLabel;
	[HideInInspector]
	public WorldObject messageReceiver;
	public string methodName;

	//On button click.
	public void OnButtonActive()
	{
		messageReceiver.SendMessage (methodName);
	}
}