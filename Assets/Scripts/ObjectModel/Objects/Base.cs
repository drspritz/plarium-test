﻿using UnityEngine;
using System.Collections;

//Base of the team. If it is destroyed, then the team owner would lose.
public class Base : Building
{
	//Base destroying delegate.
	public delegate void BaseDestroyingEventHandler (int baseGroup);
	//Base destroying event.
	public static event BaseDestroyingEventHandler BaseDestroyingEvent;

	//Destroying event caller.
	public static void OnBaseDestroy(int baseGroup)
	{		
		if (BaseDestroyingEvent != null)
		{
			BaseDestroyingEvent(baseGroup);
		}
	}

	//Call if object was destroyed.
	protected override void DestroyObject (float delay)
	{
		OnBaseDestroy (group);

		base.DestroyObject (delay);
	}

	public void Start()
	{
		Init ();
	}
}