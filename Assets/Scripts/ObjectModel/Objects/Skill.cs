﻿using UnityEngine;
using System;
using System.Collections;

//Base skill class.
[Serializable]
public class Skill
{
	public delegate void KillDelegate(WorldObject killedObject);

	//Skill activation types.
	public enum SkillActivationType
	{
		all,
		onlyEnemy,
		onlyFriends
	};

	//Skill types.
	public enum SkillType
	{
		//self,
		target,
		//aroundTarget,
		//aroundSelf,
		field
	};

	//Kill delegate.
	public KillDelegate killDelegate;

	//Skill type.
	public SkillActivationType skillActivationType;

	//Skill type.
	public SkillType skillType;

	//Skill name.
	public string name;

	//Activation target visual FX.
	public FX targetFX;

	//Magic entity FX
	public FXMagicEntity magicEntityFX;
	
	//Skill characteristics for each level.
	public SkillCharacteristics[] skillLevels;

	//Skill level.
	private int currentLevel;

	//Skill level.
	public int level
	{
		get
		{
			return currentLevel - 1;
		}
	}

	//Skill cooldown.
	public float cooldown
	{
		get
		{
			try
			{
				return skillLevels[level].cooldown;
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: skill levels not defined!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        name);
				
				return 0f;
			}
		}
	}
	
	//Skill power.
	public float power
	{
		get
		{
			try
			{
				return skillLevels[level].power;
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: skill levels not defined!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        name);
				
				return 0f;
			}
		}
	}

	//Skill cast speed.
	public float castSpeed
	{
		get
		{
			try
			{
				return skillLevels[level].castSpeed;
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: skill levels not defined!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        name);
				
				return 0f;
			}
		}
	}
	
	//Activation range.
	public float activationRange
	{
		get
		{
			try
			{
				return skillLevels[level].activationRange;
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: skill levels not defined!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        name);
				
				return 0f;
			}
		}
	}
	
	//Power radius, it is not used for single targets like self and target skill tipes.
	public float powerRange
	{
		get
		{
			try
			{
				return skillLevels[level].powerRange;
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: skill levels not defined!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        name);
				
				return 0f;
			}
		}
	}
	
	//Skill effect duration.
	public float effectDuration
	{
		get
		{
			try
			{
				return skillLevels[level].effectDuration;
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: skill levels not defined!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        name);
				
				return 0f;
			}
		}
	}
	
	//Skill effect.
	public UnitCharacteristics effect
	{
		get
		{
			try
			{
				return skillLevels[level].effect;
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0} - {1}: skill levels not defined!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        name);
				
				return null;
			}
		}
	}

	//Try level up
	public bool LevelUp()
	{
		if(PlayerManager.Instance.player.skillPoint > 0)
		{
			currentLevel++;

			if(currentLevel > skillLevels.Length)
			{
				currentLevel = Mathf.Clamp(currentLevel, 0, skillLevels.Length);

				Debug.LogFormat ("{0}: skill {1} have maximum level - {2}",
				                 System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                 name,
				                 currentLevel);

				return false;
			}

			Debug.LogFormat ("{0}: skill {1} successfuly upgraded to next level - {2}",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        name,
			                        currentLevel);
			return true;
		}
		else
		{
			Debug.LogWarningFormat ("{0}: cant upgrade skill {1}, not enogth skill points!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        name);
			return false;
		}
	}

	//Use skill, return true if object was killed.
	public bool Use(System.Object target)
	{
		bool kill = false;
		
		switch(skillType)
		{
		//case Skill.SkillType.aroundSelf:
			//Do somthing with skill.
			//break;
		//case Skill.SkillType.aroundTarget:
			//Do somthing with skill.
			//break;
		case Skill.SkillType.field:
			RaycastHit hit = new RaycastHit();

			try
			{
				hit.point = (target as SkillAttackPoint).attackPoint;
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0}: cant use skill {1}, selectedTarget is not a position!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        name);
				
				return false;
			}

			FX fx = GameObject.Instantiate(targetFX, hit.point, Quaternion.Euler(new Vector3(-90f, 0f, 0f))) as FX;
			fx.PlayFX (castSpeed);

			Collider[] colliders = Physics.OverlapSphere(hit.point, powerRange, 1 << 20);

			foreach(Collider collider in colliders)
			{
				try
				{	
					WorldObject someSelectedTarget = collider.GetComponent<WorldObject>();

					//Filter.
					switch(skillActivationType)
					{
					case Skill.SkillActivationType.onlyEnemy:
						if(someSelectedTarget.group == PlayerManager.Instance.playerGroup || !someSelectedTarget.isVisibleForAI)
							continue;
						break;
					case Skill.SkillActivationType.onlyFriends:
						if(someSelectedTarget.group != PlayerManager.Instance.playerGroup || !someSelectedTarget.isVisibleForAI)
							continue;
						break;
					}

					if(someSelectedTarget.ApplyDamage(power, castSpeed))
					{
						killDelegate(someSelectedTarget);
					}
					someSelectedTarget.ApplyEffect(skillLevels[level]);
				}
				catch(Exception ex)
				{
					Debug.LogWarningFormat (ex.Message);
					Debug.LogWarningFormat ("{0}: cant use skill {1}, selectedTarget is not a world object!",
					                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
					                        name);
				}
			}
			break;
		//case Skill.SkillType.self:
			//Do somthing with skill.
			//break;
		case Skill.SkillType.target:
			
			WorldObject selectedTarget = null;
			
			try
			{
				selectedTarget = target as WorldObject;
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat (ex.Message);
				Debug.LogWarningFormat ("{0}: cant use skill {1}, selectedTarget is not a world object!",
				                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
				                        name);
				
				return false;
			}

			if(targetFX != null)
			{
				FX targetfx = GameObject.Instantiate(targetFX, selectedTarget.transform.position, Quaternion.Euler(new Vector3(-90f, 0f, 0f))) as FX;
				targetfx.PlayFX (selectedTarget, castSpeed);
			}

			if(magicEntityFX != null)
			{
				FXMagicEntity magicFX = GameObject.Instantiate(magicEntityFX) as FXMagicEntity;
				magicFX.StartEntity (PlayerManager.Instance.player, selectedTarget, castSpeed);
			}

			kill = selectedTarget.ApplyDamage(power, castSpeed);

			if(kill)
			{
				killDelegate(selectedTarget);
			}

			selectedTarget.ApplyEffect(skillLevels[level]);
			break;
		}
		
		return kill;
	}
}