﻿using UnityEngine;
using System;
using System.Collections;

//Base unit model.
[RequireComponent(typeof(NavMeshAgent))]
public class Unit : WorldObject, IControllable, IHealed
{
	//Unit modes.
	public enum UnitMode
	{
		backToBase,
		moveToDestination,
		attack,
		wait
	};

	//Barrack owner, which created this unity.
	private Barrack currentOwner;

	//Current unit target.
	protected WorldObject currentTarget;

	//Navmesh agent.
	protected NavMeshAgent navMeshAgen;

	//Timer for calculate attack speed.
	private float attackSpeedTimer;

	//Barrack level up delegate.
	public delegate void UnitDestroyEventHandler (Unit unit);
	//Barrack level up event.
	public static event UnitDestroyEventHandler UnitDestroyEvent;

	//Unit initial (edit in inspector) characteristics.

	//Unit initial attack power.
	public float initialAttack;
	//Unit initial attack speed.
	public float initialAttackSpeed;
	//Unit initial move speed.
	public float initialSpeed;
	//Unit initial attack range.
	public float initialAttackRange;
	//Unit initial gold.
	public int initialGold;
	//Unit initial experience.
	public int initialExp;

	//Death effect.
	public FX deathFx;
	//Attack effect.
	public FX attackFX;

	//Unit current (hidden for public) characteristics.

	//Unit current attack power.
	private float currentAttack;
	//Unit current attack speed.
	private float currentAttackSpeed;
	//Unit current move speed.
	private float currentSpeed;
	//Unit current attack range.
	private float currentAttackRange;
	//Unit current gold.
	private int currentGold;
	//Unit current experience.
	private int currentExp;

	//Current unit modes.
	public UnitMode currentMode = UnitMode.moveToDestination;

	//For restore characteristics after effects.
	protected float oldAttack;
	protected float oldAttackSpeed;
	protected float oldSpeed;
	protected float oldAttackRange;
	protected int oldGold;
	protected int oldExp;

	//Unit public properties.

	//Attack getter and setter for public.
	public float attack
	{
		get
		{
			return currentAttack;
		}
		protected set
		{
			if(currentAttack != value)
			{
				currentAttack = Mathf.Clamp(value, 0f, 99999f);
			}
		}
	}

	//Attack speed getter and setter for public.
	public float attackSpeed
	{
		get
		{
			return currentAttackSpeed;
		}
		protected set
		{
			if(currentAttackSpeed != value)
			{
				currentAttackSpeed = Mathf.Clamp(value, 0f, 99999f);
			}
		}
	}

	//Speed getter and setter for public.
	public float speed
	{
		get
		{
			return currentSpeed;
		}
		protected set
		{
			if(currentSpeed != value)
			{
				currentSpeed = Mathf.Clamp(value, 0f, 99999f);
			}
		}
	}

	//Attack range getter and setter for public.
	public float attackRange
	{
		get
		{
			return currentAttackRange;
		}
		protected set
		{
			if(currentAttackRange != value)
			{
				currentAttackRange = Mathf.Clamp(value, 0f, 99999f);
			}
		}
	}

	//Gold getter and setter for public.
	public int gold
	{
		get
		{
			return currentGold;
		}
		protected set
		{
			if(currentGold != value)
			{
				currentGold = Mathf.Clamp(value, 0, 99999);
			}
		}
	}

	//Exp getter and setter for public.
	public int exp
	{
		get
		{
			return currentExp;
		}
		protected set
		{
			if(currentExp != value)
			{
				currentExp = Mathf.Clamp(value, 0, 99999);
			}
		}
	}

	//Get nav mesh speed.
	public Vector3 navMeshVelocity
	{
		get
		{
			return navMeshAgen.velocity;
		}
	}

	//Get current target.
	public WorldObject getCurrentTarget
	{
		get
		{
			return currentTarget;
		}
	}

	//Extend init method.
	public override void Init ()
	{
		base.Init ();

		try
		{
			navMeshAgen = GetComponent<NavMeshAgent>();
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: the object has not have a nav mesh agent!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);

			enabled = false;
			return;
		}

		attack = initialAttack;
		attackSpeed = initialAttackSpeed;
		speed = initialSpeed;
		attackRange = initialAttackRange;
		gold = initialGold;
		exp = initialExp;

		navMeshAgen.speed = speed;

		currentMode = UnitMode.wait;
	}

	//First configure.
	public void Configure(WorldObjectCharacteristics characteristcs, Barrack owner)
	{
		currentOwner = owner;
		currentOwner.BarrackLevelUpEvent += OnUnitUpgrade;

		OnUnitUpgrade (owner.level, characteristcs);
	}

	//Remember current characteristics.
	protected override void RememberCharacteristics()
	{
		base.RememberCharacteristics ();

		oldAttack = attack;
		oldAttackSpeed = attackSpeed;
		oldSpeed = speed;
		oldAttackRange = attackRange;
		oldGold = gold;
		oldExp = exp;
	}
	
	//Restore old characteristics.
	protected override void RestoreCharacteristics()
	{
		base.RestoreCharacteristics ();

		attack = oldAttack;
		attackSpeed = oldAttackSpeed;
		speed = oldSpeed;
		attackRange = oldAttackRange;
		gold = oldGold;
		exp = oldExp;
		
		navMeshAgen.speed = speed;
	}

	//Extend effects.
	protected override IEnumerator ApplyEffectProcess (SkillCharacteristics skillCharacteristics)
	{
		yield return new WaitForSeconds (skillCharacteristics.castSpeed);

		UnitCharacteristics inputEffect = skillCharacteristics.effect as UnitCharacteristics;
		
		//Apply effect
		maxHp *= inputEffect.hp;
		armor *= inputEffect.armor;
		attack *= inputEffect.attack;
		attackSpeed *= inputEffect.attackSpeed;
		speed *= inputEffect.speed;
		attackRange *= inputEffect.attackRange;
		gold *= inputEffect.gold;
		exp *= inputEffect.exp;
		
		//Clamp HP.
		hp = Mathf.Clamp(hp, 0f, maxHp);

		//Apply navmesh new speed.
		navMeshAgen.speed = speed;
		
		//Wait until effect works.
		yield return new WaitForSeconds (skillCharacteristics.effectDuration);

		RestoreCharacteristics ();
	}

	//Upgrade unit.
	protected override void OnUnitUpgrade(int newLevel, WorldObjectCharacteristics characteristcs)
	{
		base.OnUnitUpgrade (newLevel, characteristcs);

		UnitCharacteristics inputCharacteristics = new UnitCharacteristics ();

		try
		{
			inputCharacteristics = characteristcs as UnitCharacteristics;
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: input characteristics has broken!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);

			return;
		}

		armor = initialArmor + inputCharacteristics.armor;
		attack = initialAttack + inputCharacteristics.attack;
		attackSpeed = initialSpeed + inputCharacteristics.attackSpeed;
		speed = initialSpeed + inputCharacteristics.speed;
		attackRange = initialAttackRange + inputCharacteristics.attackSpeed;
		gold = initialGold + inputCharacteristics.gold;
		exp = initialExp + inputCharacteristics.exp;

		navMeshAgen.speed = speed;

		RememberCharacteristics ();
	}

	//Unit destroy event caller.
	private void OnUnitDestroy()
	{		
		if (UnitDestroyEvent != null)
		{
			UnitDestroyEvent(this);
		}
	}

	//Set destination point.
	public virtual void SetDestination (Vector3 dest)
	{
		try
		{
			currentMode = UnitMode.moveToDestination;

			if(navMeshAgen != null && navMeshAgen.enabled && navMeshAgen.isOnNavMesh)
			{
				navMeshAgen.SetDestination(dest);
			}
			else
			{
				Debug.LogWarningFormat ("{0} - {1}: nav mesh agent is disabled, switch to wait mode!",
				                        DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name);
				
				currentMode = UnitMode.wait;
				return;
			}
		}
		catch(System.Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: the object has not have a nav mesh agent, unit will be switch to wait mode!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);

			currentMode = UnitMode.wait;
		}
	}

	//Send unit to base.
	public virtual void BackToBase()
	{
		Vector3 baseDestination = Vector3.zero;

		try
		{
			baseDestination = MissionManager.Instance.GetBasePosition (this);
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: PlayerManager instance not initialized!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
			
			enabled = false;
			return;
		}

		try
		{
			if(navMeshAgen != null && navMeshAgen.enabled && navMeshAgen.isOnNavMesh)
			{
				navMeshAgen.SetDestination(baseDestination);
			}
			else
			{
				Debug.LogWarningFormat ("{0} - {1}: nav mesh agent is disabled, switch to wait mode!",
				                        DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name);

				currentMode = UnitMode.wait;
				return;
			}
		}
		catch(System.Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: the object has not have a nav mesh agent, unit will be switch to wait mode!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);

			currentMode = UnitMode.wait;
			return;
		}
	}

	//Set target.
	public virtual void SetTarget (WorldObject target)
	{
		try
		{
			if(target.isVisibleForAI)
			{
				currentTarget = target;
				currentMode = UnitMode.moveToDestination;
			}
			else
			{
				Debug.LogWarningFormat ("{0} - {1}: target is invisible!",
				                        DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name);
				
				currentMode = UnitMode.wait;
				return;
			}
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: target is not a world object!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
			
			currentMode = UnitMode.wait;
			return;
		}
	}

	//Unset current target.
	public virtual void UnsetTarget ()
	{
		currentTarget = null;

		if(navMeshAgen != null && navMeshAgen.enabled && navMeshAgen.isOnNavMesh)
		{
			navMeshAgen.SetDestination(transform.position);
			currentMode = UnitMode.wait;
		}
		else
		{
			Debug.LogWarningFormat ("{0} - {1}: nav mesh agent is disabled, switch to wait mode!",
			                        DateTime.Now.ToString ("HH:mm:ss:fff"),
			                        gameObject.name);
			
			currentMode = UnitMode.wait;
			return;
		}
	}

	//Attack current target.
	private void Attack()
	{
		bool lastHit = false;

		try
		{
			lastHit = currentTarget.ApplyDamage(attack, 0.01f);
			OnAttack(currentTarget);
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: cant apply damage, nothing to attack!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);

			currentMode = UnitMode.wait;
			return;
		}

		if(lastHit)
		{
			OnKill(currentTarget);
			currentMode = UnitMode.wait;
		}
	}

	//Calculate unit logic.
	protected virtual void UnitProcess()
	{
		//Attack speed timer.
		attackSpeedTimer += Time.deltaTime;

		switch(currentMode)
		{
			//Nothing to do? Stop work and wait any tasks.
		case UnitMode.wait:
			break;  
			//Attack mode, calculate attak speed and distance to current target.
		case UnitMode.attack:

			float attackDistance = Mathf.Infinity;

			if(currentTarget != null)
			{
				attackDistance = Vector3.Distance(transform.position, currentTarget.transform.position);
			}
			else
			{
				currentMode = UnitMode.wait;
				return;
			}

			if(attackDistance < attackRange && currentTarget.group != 0)
			{
				if(navMeshAgen != null && navMeshAgen.enabled)
				{
					navMeshAgen.SetDestination(transform.position);
				}
				else
				{
					Debug.LogWarningFormat ("{0} - {1}: nav mesh agent is disabled, switch to wait mode!",
					                        DateTime.Now.ToString ("HH:mm:ss:fff"),
					                        gameObject.name);
					
					currentMode = UnitMode.wait;
					return;
				}

				float attackTimeTrigger = 1f / attackSpeed;

				if(attackSpeedTimer > attackTimeTrigger)
				{
					if(currentTarget.group == 0) // Check live target.
					{
						currentMode = UnitMode.wait;
						return;
					}

					Attack();
					attackSpeedTimer = 0f;
				}

				Vector3 relativePos = currentTarget.transform.position - transform.position;
				transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(relativePos), 10f * Time.deltaTime);
			}
			else
			{
				currentMode = UnitMode.moveToDestination;
			}
			break;
			//Go to base.
		case UnitMode.backToBase:
			if(navMeshAgen != null && navMeshAgen.enabled)
			{
				//Have target? Go for it.
				if(currentTarget != null)
				{
					if(navMeshAgen != null && navMeshAgen.enabled)
					{
						navMeshAgen.SetDestination(currentTarget.transform.position);
					}
					else
					{
						Debug.LogWarningFormat ("{0} - {1}: nav mesh agent is disabled, switch to wait mode!",
						                        DateTime.Now.ToString ("HH:mm:ss:fff"),
						                        gameObject.name);
						
						currentMode = UnitMode.wait;
						return;
					}
					
					//Check target distance, it may be possible to attack?
					float distance = Vector3.Distance(transform.position, currentTarget.transform.position);
					
					if(distance < attackRange)
					{
						currentMode = UnitMode.attack;
					}
				}
				//Continue go to destination.
				else
				{
					//Arrived at destination? Wait new tasks.
					if (!navMeshAgen.pathPending)
						if (navMeshAgen.remainingDistance <= navMeshAgen.stoppingDistance)
							if (!navMeshAgen.hasPath || navMeshAgen.velocity.sqrMagnitude == 0f)
								currentMode = UnitMode.wait;
				}
			}
			else
			{
				Debug.LogWarningFormat ("{0} - {1}: nav mesh agent is disabled, switch to wait mode!",
				                        DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name);
				
				currentMode = UnitMode.wait;
				return;
			}
			break;
			//Check target and move to destination.
		case UnitMode.moveToDestination:
			if(navMeshAgen != null && navMeshAgen.enabled)
			{
				//Have target? Go for it.
				if(currentTarget != null)
				{
					if(currentTarget.group != 0) //If mob not killed or somthing else.
					{
						if(navMeshAgen != null && navMeshAgen.enabled)
						{
							navMeshAgen.SetDestination(currentTarget.transform.position);
						}
						else
						{
							Debug.LogWarningFormat ("{0} - {1}: nav mesh agent is disabled, switch to wait mode!",
							                        DateTime.Now.ToString ("HH:mm:ss:fff"),
							                        gameObject.name);
							
							currentMode = UnitMode.wait;
							return;
						}

						//Check target distance, it may be possible to attack?
						float distance = Vector3.Distance(transform.position, currentTarget.transform.position);

						if(distance < attackRange)
						{
							currentMode = UnitMode.attack;
						}
					}
				}
				//Continue go to destination.
				else
				{
					//Arrived at destination? Wait new tasks.
					if (!navMeshAgen.pathPending)
						if (navMeshAgen.remainingDistance <= navMeshAgen.stoppingDistance)
							if (!navMeshAgen.hasPath || navMeshAgen.velocity.sqrMagnitude == 0f)
								currentMode = UnitMode.wait;
				}
			}
			else
			{
				Debug.LogWarningFormat ("{0} - {1}: nav mesh agent is disabled, switch to wait mode!",
				                        DateTime.Now.ToString ("HH:mm:ss:fff"),
				                        gameObject.name);

				currentMode = UnitMode.wait;
				return;
			}
			break;
		}
	}

	//Heal unit.
	public void Heal (float restoreValue, int healerGroup)
	{
		if (healerGroup == group)
			hp += restoreValue;
	}

	//Call if object was destroyed.
	protected override void DestroyObject (float delay)
	{
		base.DestroyObject (delay);

		try
		{
			FX fx = Instantiate (deathFx, transform.position, Quaternion.Euler(new Vector3(-90f, 0f, 0f))) as FX;
			fx.PlayFX (delay);
		}
		catch(System.Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: deathFx not defined!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
			
			return;
		}

		enabled = false;
		Destroy (gameObject, delay);
		
		OnUnitDestroy();
	}

	//Cleaning unit event subscription.
	public void OnDestroy()
	{
		try
		{
			currentOwner.BarrackLevelUpEvent -= OnUnitUpgrade;
			currentOwner.OnUnitDeath();
		}
		catch(Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: the object not needed to usubscribe, beacause owner not exist!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
		}
	}

	//On attack.
	protected virtual void OnAttack(WorldObject attackObject)
	{
		try
		{
			FX fx = Instantiate (attackFX, attackObject.transform.position, Quaternion.Euler(new Vector3(-90f, 0f, 0f))) as FX;
			fx.PlayFX (transform.position, 0f);
		}
		catch(System.Exception ex)
		{
			Debug.LogWarningFormat (ex.Message);
			Debug.LogWarningFormat ("{0} - {1}: deathFx not defined!",
			                        System.DateTime.Now.ToString("HH:mm:ss:fff"),
			                        gameObject.name);
			
			return;
		}
	}

	//On kill somthing.
	public virtual void OnKill(WorldObject killedObject) {}

	//Get class info.
	public override string ToString ()
	{
		string oldString = base.ToString ();

		return string.Format (oldString + 
		                      "Attack: {0}\n" +
		                      "AttackSpeed: {1}\n" +
		                      "Speed: {2}\n" +
		                      "AttackRange: {3}\n" +
		                      "Gold: {4}\n" +
		                      "Exp: {5}\n",
		                      attack,
		                      attackSpeed,
		                      speed,
		                      attackRange,
		                      gold,
		                      exp);
	}
}