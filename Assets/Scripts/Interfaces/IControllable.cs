﻿using UnityEngine;
//All objects, which implement this interface can be controllable.
public interface IControllable
{
	void SetDestination(Vector3 dest);
	void BackToBase();
	void SetTarget(WorldObject target);
	void UnsetTarget();
	void OnKill(WorldObject killedObject);
}