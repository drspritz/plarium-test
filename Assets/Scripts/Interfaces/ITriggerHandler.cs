﻿using UnityEngine;
//All objects, which implement this interface can receive trigger callback from the custom collider.
public interface ITriggerHandler
{
	void OnCustomTriggerStay (Collider other);
}