﻿//All objects, which implement this interface can be healed.
public interface IHealed
{
	void Heal(float restoreValue, int healerGroup);
}