﻿//All objects, which implement this interface can receive damage.
public interface IDamageable
{
	bool ApplyDamage(float damageValue, float delay);
}