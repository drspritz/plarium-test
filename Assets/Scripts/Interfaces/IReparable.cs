﻿//All objects, which implement this interface can be repaired.
public interface IReparable
{
	void Repair(float restoreValue, int group);
}