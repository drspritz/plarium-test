﻿using UnityEngine;
using System.Collections;

public class FXMagicEntity : MonoBehaviour
{
	private WorldObject initiator, target;
	private float startTime, endTime, timeToHit;

	public Transform detachTail;

	public void StartEntity(WorldObject _initiator, WorldObject _target, float _timeToHit)
	{
		startTime = Time.time;

		initiator = _initiator;
		target = _target;
		timeToHit = _timeToHit;

		endTime = startTime + timeToHit;

		StartCoroutine (TailDetachTimer(timeToHit));
	}

	public void Update()
	{
		float progress = 1f - (endTime - Time.time) / timeToHit;

		if(target != null)
		{
			transform.position = Vector3.Lerp (initiator.transform.position, target.transform.position, progress);

			Vector3 relativePos = target.transform.position - transform.position;
			transform.rotation = Quaternion.LookRotation(relativePos);
		}
	}

	public IEnumerator TailDetachTimer(float delay)
	{
		yield return new WaitForSeconds(delay);

		if(detachTail != null)
		{
			detachTail.SetParent (null);
			GameObject.Destroy(detachTail.gameObject, 3f);
		}

		Destroy (gameObject);
	}
}