﻿using UnityEngine;
using System.Collections;

public class FXCameraNoiseGenerator : MonoBehaviour
{
	private static float noiseIntensity;

	public GameCameraController cameraController;
	
	public float noiseDelay;

	private float startTime;

	public static void AddIntensity(float intensity)
	{
		noiseIntensity += intensity;
		Mathf.Clamp(noiseIntensity, 0f, 1f);
	}

	private Vector3 CreateRandomVector()
	{
		return new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
	}

	public void Update()
	{
		if(Time.time > startTime + noiseDelay)
		{
			cameraController.SetRandomPosition(CreateRandomVector() * noiseIntensity);

			noiseIntensity = 0f;

			startTime = Time.time;
		}
	}
}