﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class FXSpaceParticles : MonoBehaviour
{
	private ParticleSystem _particleSystem;

	void Awake()
	{
		_particleSystem = GetComponent<ParticleSystem>();
	}

	void LateUpdate()
	{
		ParticleSystem.Particle[] particles = new ParticleSystem.Particle[_particleSystem.particleCount];

		_particleSystem.GetParticles (particles);

		for(int i = 0; i < _particleSystem.particleCount; i++)
		{
			float distance = Vector3.Distance(transform.position, particles[i].position);

			if(distance > 20)
			{
				particles[i].startLifetime = 0f;
			}
		}

		_particleSystem.SetParticles (particles, _particleSystem.particleCount);
	}
}