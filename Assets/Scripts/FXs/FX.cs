﻿using UnityEngine;
using System.Collections;

public class FX : MonoBehaviour
{
	public string stateName;
	public Animator anim;
	public ParticleSystem particles;
	public LineRenderer line;

	public float noiseIntensity;
	public float noiseDuration;

	public float lineDisableDelay;

	//If zero - destroy disabled.
	public float timeLife;

	//Start noise time.
	private float startNoiseTime;
	//End noise time.
	private float endNoiseTime;
	
	public void PlayFX(float startDelay)
	{
		if(gameObject.activeInHierarchy)
			StartCoroutine (PlayFXCoroutine(null, Vector3.zero, startDelay));
	}

	public void PlayFX(WorldObject target, float startDelay)
	{
		if(gameObject.activeInHierarchy)
			StartCoroutine (PlayFXCoroutine(target, Vector3.zero, startDelay));
	}

	public void PlayFX(Vector3 startPosition, float startDelay)
	{
		if(gameObject.activeInHierarchy)
			StartCoroutine (PlayFXCoroutine(null, startPosition, startDelay));
	}

	private IEnumerator PlayFXCoroutine(WorldObject target, Vector3 startPosition, float startDelay)
	{
		yield return new WaitForSeconds (startDelay);

		if (line != null)
		{
			line.SetPosition (0, startPosition);
			line.SetPosition (1, transform.position);
		}

		if(anim != null)
		{
			anim.Play(stateName, 0, 0f);
		}
		
		startNoiseTime = Time.time;
		endNoiseTime = startNoiseTime + noiseDuration;

		if (target != null)
			transform.position = target.transform.position;

		if (particles != null)
			particles.Play ();
		
		if(timeLife > 0f)
			Destroy (gameObject, timeLife);

		if (line != null)
		{
			yield return new WaitForSeconds (lineDisableDelay);
			line.gameObject.SetActive (false);
		}
	}

	public void Update()
	{
		if(endNoiseTime > Time.time)
		{
			float normalizedTime = (endNoiseTime - Time.time) / noiseDuration;
			float noiseIntensityValue = Mathf.Lerp(noiseIntensity, 0f, normalizedTime);

			FXCameraNoiseGenerator.AddIntensity (noiseIntensityValue);
		}
	}
}