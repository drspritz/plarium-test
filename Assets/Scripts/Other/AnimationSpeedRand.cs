﻿using UnityEngine;
using System.Collections;

public class AnimationSpeedRand : MonoBehaviour
{
	public float speedFactor;

	IEnumerator Start ()
	{
		yield return new WaitForSeconds (Random.Range(0f, 0.3f));

		Animator anim = GetComponent<Animator> ();

		if (anim)
		{
			anim.speed = Random.Range (0.8f, 1f) * speedFactor;
		}
	}
}